<?php

/**
 * @file
 * Facetapi query type for intervals.
 */

/**
 * Facetapi query type for intervals.
 */
class SearchApiFacetapiIntervals extends SearchApiFacetapiTerm implements FacetapiQueryTypeInterface {

  /**
   * {@inheritdoc}
   */
  static public function getType() {
    return 'intervals';
  }

  /**
   * {@inheritdoc}
   */
  public function execute($query) {
    // Add query filter by calling parent class -- it will call $this->addFacetFilter().
    parent::execute($query);

    // Get context.
    $field = $this->facet['field'];
    $intervals = $this->getIntervals();

    // If no interval has been defined for this facet, log warning and return.
    if (empty($intervals)) {
      watchdog('facetapi_intervals', 'No interval defined for field %field: this facet will be rendered as a regular facet.', array('%field' => $field), WATCHDOG_WARNING);
      return;
    }

    // Otherwise, add interval facet.
    $facet_intervals = $query->getOption('facet_intervals', array());
    $facet_intervals[$field] = $intervals;
    $query->setOption('facet_intervals', $facet_intervals);

    // And remove regular facet.
    $facets = $query->getOption('search_api_facets', array());
    unset($facets[$field]);
    $query->setOption('search_api_facets', $facets);
  }

  /**
   * {@inheritdoc}
   */
  protected function addFacetFilter($query_filter, $field, $filter) {
    // Verify the filter is valid for this field.
    $intervals = $this->getIntervals();
    if (!isset($intervals[$filter])) {
      watchdog('facetapi_intervals', 'No interval defined for field %field with value %filter: this active facet will be ignored.', array('%field' => $field, '%filter' => $filter), WATCHDOG_WARNING);
      return;
    }

    // Add interval filter query by calling the parent class.
    list($start, $end) = $intervals[$filter];
    parent::addFacetFilter($query_filter, $field, "[$start TO $end]");
  }

  /**
   * Get list of intervals for this facet.
   */
  public function getIntervals() {
    $intervals_text = $this->getSettings()->settings['intervals'];
    $intervals_array = array();
    if (!empty($intervals_text)) {
      $lines = preg_split('/[\r\n]+/', $intervals_text);
      // For each interval, extract the name, start and end bounds.
      foreach ($lines as $line) {
        if (!empty($line)) {
          list($name, $start, $end) = explode('|', $line, 3);
          $intervals_array[$name] = array($start, $end);
        }
      }
    }
    return $intervals_array;
  }

}
