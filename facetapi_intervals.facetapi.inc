<?php

/**
 * @file
 * Facet API hook implementations.
 */

/**
 * Implements hook_facetapi_query_types().
 *
 * You will need to associate the facet with:
 * - Our custom query type 'intervals'
 * - The contrib widget 'facetapi_checkbox_links'
 *
 * There is no hook to alter the contrib widget and tell it it supports our custom query type.
 * As a result you cannot save the facet settings via the UI at admin/config/search/facetapi/<index>/block/<field>/edit
 *
 * A possible workaround is to create a brand new widget with the only purpose of associating the contrib widget with our custom query type:
 *   function facetapi_intervals_facetapi_widgets() {
 *     return array(
 *       'facetapi_intervals_checkbox_links' => array(
 *         'handler' => array(
 *           'label' => t('Links with checkboxes (Intervals)'),
 *           'class' => 'FacetapiWidgetCheckboxLinks',
 *           'query types' => array('intervals'),
 *         ),
 *       );
 *     );
 *   }
 *
 * The downside would be that the pubdate facet will be rendered with CSS classes different from other facets and as a result would require custom CSS styling:
 *   <ul class="facetapi-facetapi-intervals-checkbox-links-intervals">
 *   <!-- versus -->
 *   <ul class="facetapi-facetapi-checkbox-links">
 *
 * @see facetapi_facetapi_widgets()
 */
function facetapi_intervals_facetapi_query_types() {
  return array(
    'intervals' => array(
      'handler' => array(
        'class' => 'SearchApiFacetapiIntervals',
        'adapter' => 'search_api',
      ),
    ),
  );
}

